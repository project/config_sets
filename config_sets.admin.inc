<?php

use Drupal\configuration\Config\ConfigurationManagement;
use Drupal\configuration\Utils\ConfigIteratorSettings;

function config_sets_overview() {

  $header = array(
    t('Configuration Set'),
    t('Status'),
    t('Operations'),
  );
  $rows = array();

  $config_sets = array();

  // Start looking at sites/all/configurations
  foreach (array('sites/all/configurations', conf_path() . '/configurations') as $dir) {
    if (is_dir(drupal_realpath($dir))) {
      foreach (glob($dir . "/*") as $folder) {
        if (is_dir(drupal_realpath($folder)) && is_file($folder . '/configurations.inc')) {
          unset($info);

          $file_content = drupal_substr(file_get_contents($folder . '/configurations.inc'), 6);
          eval($file_content);
          if (isset($info)) {
            $rows[] = array('name' => $info['name'] . " ({$info['machine_name']})", 'status' => 'In Sync', 'operations' => 'view');
          }
        }
      }
    }
  }

  return theme('table', array('header' => $header, 'rows' => $rows));

}

function config_sets_create_form($form, &$form_state) {
  $configurations = ConfigurationManagement::allConfigurations();

  $form_state['table_header'] = array(
    'names' => t('Configuration'),
    'status' => t('Tracked'),
    'operations' => t('Operations'),
  );

  config_sets_configuration_list('export', $configurations, $form, $form_state);
  $form['info']['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#required' => TRUE,
    '#weight' => -10,
  );

  $form['info']['machine_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Machine Name'),
    '#required' => TRUE,
    '#weight' => -9,
  );

  $form['info']['description'] = array(
    '#type' => 'textfield',
    '#title' => t('Description'),
    '#weight' => -8,
    '#description' => t('An optional text to explain what this set contains.')
  );

  $form['info']['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Create Set'),
    '#submit' => array('config_sets_create_form_submit'),
  );
  return $form;
}

/**
 * Helper function for configuration lists.
 */
function config_sets_configuration_list($ui, $configurations, &$form, &$form_state) {
  $component_exists = FALSE;
  $handlers = configuration_configuration_handlers();

  $form['#attached']['css'] = array(
    drupal_get_path('module', 'config_sets') . '/config_sets.css'
  );

/*
  $form['#attached']['js'] = array(
    drupal_get_path('module', 'configuration_ui') . '/js/configuration_ui.js'
  );*/

  $form['info'] = array(
    '#prefix' => '<div class = "config-sets-info">',
    '#suffix' => '</div>',
  );

  $form['components'] = array(
    '#prefix' => '<div class = "config-sets-components">',
    '#suffix' => '</div>',
  );

  $form['components']['auto_detect'] = array(
    '#type' => 'checkbox',
    '#title' => t('Auto detect dependencies'),
    '#default_value' => TRUE,
    '#description' => t('If checked, every time a configuration is selected, the dependencies will be automatically detected and selected too.'),
  );


  $form_state['component_exists'] = FALSE;
  foreach ($configurations as $component => $list) {
    $handler = ConfigurationManagement::getConfigurationHandler($component);
    if (empty($handler) || count($list) < 1) {
      continue;
    }
    $form['components'][$component] = array(
      '#type' => 'fieldset',
      //'#group' => 'packages',
      '#title' => check_plain($handler::getComponentHumanName($component)),
      '#description' => '<span class= "component" rel="' . check_plain($component) . '">' .  t('Configurations for: @component', array('@component' => $handler::getComponentHumanName($component, TRUE))) . '</span>',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#tree' => TRUE,
      // '#attached' => array(
      //   'js' => array(
      //     'vertical-tabs' => drupal_get_path('module', 'configuration_ui') . '/theme/vertical-tabs.js',
      //   ),
      // ),
      '#attributes' => array('class' => array('configuration')),
    );

    $form['components'][$component]['configurations'] = array(
      '#tree' => FALSE,
      '#theme' => 'configuration_table',
      'items' => array(
        '#tree' => TRUE,
      ),
      'name' => array(
        '#tree' => TRUE,
      ),
      'status' => array(
        '#tree' => TRUE,
      ),
      'operations' => array(
        '#tree' => TRUE,
      ),
    );

    $form['components'][$component]['configurations']['#header'] = $form_state['table_header'];

    foreach ($list as $identifier => $info) {
      $form_state['component_exists'] = TRUE;
      $id = $component . '.' . $identifier;

      $form['components'][$component]['configurations']['items'][$id] = array(
        '#type' => 'checkbox',
        '#title' => check_plain($info['name']),
        '#title_display' => 'invisible',
        '#return_value' => $id,
      );

      $form['components'][$component]['configurations']['names'][$id] = array(
        '#markup' => check_plain($info['name']),
      );

      if ($ui == 'tracking') {
        $form['components'][$component]['configurations']['status'][$id] = array(
          '#markup' => '<span class ="config-status" rel="' . $id . '">' . t('Processing...') . '</span>',
        );
      }
      elseif ($ui == 'export') {
        $form['components'][$component]['configurations']['status'][$id] = array(
          '#markup' => $info['hash'] ? t('Tracked') : t('Not tracked'),
        );
      }

      $form['components'][$component]['configurations']['operations'][$id] = array(
        '#markup' => $id,
      );
    }
    asort($form['components'][$component]['configurations']['items']);
  }

  $form['info']['dependencies'] = array(
    '#type' => 'fieldset',
    '#title' => t('Set Dependencies'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $modules =  array();
  foreach (system_get_info('module') as $module_name => $module_info) {
    $modules[$module_name] = check_plain($module_info['name']) . " ($module_name)";
  }

  $form['info']['dependencies']['modules'] = array(
    '#type' => 'checkboxes',
    '#options' => $modules,
    '#size' => 20,
  );
}

function config_sets_create_form_submit($form, $form_state) {
  $export_dependencies = $export_optionals = FALSE;
  $list = array_keys(array_filter($form_state['values']['items']));

  if (empty($list)) {
    drupal_set_message(t('You must select at least one configuration to create the set of configurations'), 'error');
    return;
  }

  $settings = new ConfigIteratorSettings(
      array(
        'build_callback' => 'build',
        'callback' => 'printRaw',
        'process_dependencies' => $export_dependencies,
        'process_optionals' => $export_optionals,
        'info' => array(
          'exported' => array(),
          'exported_files' => array(),
          'hash' => array(),
          'modules' => array(),
        ),
        'settings' => array(
          'format' => 'tar',
        )
      )
    );

    module_invoke_all('configuration_pre_export', $settings);

    $filename = $form_state['values']['machine_name'] . '.tar';

    // Clear out output buffer to remove any garbage from tar output.
    if (ob_get_level()) {
      ob_end_clean();
    }

    drupal_add_http_header('Content-type', 'application/x-tar');
    drupal_add_http_header('Content-Disposition', 'attachment; filename="' . $filename . '"');
    drupal_send_headers();

    $hashes = array();

    foreach ($list as $component) {
      $config = ConfigurationManagement::createConfigurationInstance($component);

      // Make sure the object is built before start to iterate on its
      // dependencies.
      $config->setContext($settings);
      $config->build();
      $config->buildHash();
      $hashes[$config->getUniqueId()] = $config->getHash();
      $config->iterate($settings);
    }

    $exported = $settings->getInfo('exported');

    module_invoke_all('configuration_post_export', $settings);

    $file_content = "<?php\n\n";
    $file_content .= "// This file contains the list of configurations contained in this set.\n\n";
    $file_content .= "\$info = array(\n";
    $file_content .= "  'name' => '" . check_plain($form_state['values']['name']) . "',\n";
    $file_content .= "  'machine_name' => '" . check_plain($form_state['values']['machine_name']) . "',\n";
    $file_content .= "  'description' => '" . check_plain($form_state['values']['description']) . "',\n";
    $file_content .= ");\n\n";

    $file_content .= "// This file contains the list of configurations contained in this set.\n\n";
    $file_content .= "\$configurations = array(\n";
    foreach ($hashes as $config => $hash) {
      $file_content .= "  '$config' => '$hash',\n";
    }
    $file_content .= ");\n\n";
    $file_content .= "\$modules = array(\n";
    foreach (array_unique($settings->getInfo('modules')) as $module) {
      $file_content .= "  '$module',\n";
    }
    $file_content .= ");\n";

    print ConfigurationManagement::createTarContent($form_state['values']['machine_name'] . "/configurations.inc", $file_content);

    print pack("a1024", "");
    exit;
}
